FROM nginx

# set the working directory in the container
WORKDIR /code

# copy the dependencies file to the working directory
COPY index.html /usr/share/nginx/html

COPY nginx.conf /etc/nginx